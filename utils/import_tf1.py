import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow import __version__ as tf_version
if tf_version.split('.')[0] == '2':
    from tensorflow.compat.v1 import *
    disable_v2_behavior()
else:
    from tensorflow import *
