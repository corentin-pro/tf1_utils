import os

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

from tensorflow import *
# Fix bug with RTX GPUs
gpus = config.experimental.list_physical_devices('GPU')
if gpus:
    try:
        for gpu in gpus:
            config.experimental.set_memory_growth(gpu, True)
    except RuntimeError as error:
        print(error)
