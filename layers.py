import src.tf1_utils.utils.import_tf1 as tf

from .utils.logger import DummyLogger


class LayerInfo():
    def __init__(self):
        self.memory = 0.0
        self.ops = 0.0
        self.output = 0.0


class LayerSummaries():
    def __init__(self):
        self.scalars = []
        self.histograms = []
        self.images = []


def l1_loss(tensor, scope=None):
    with tf.variable_scope(scope, 'L1Loss', [tensor]):
        loss = tf.reduce_sum(tf.abs(tensor))
        tf.add_to_collection(tf.GraphKeys.REGULARIZATION_LOSSES, loss)
        return loss


class Layer():
    # Default layer arguments
    ACTIVATION = tf.nn.relu

    BATCH_NORM = True
    BATCH_NORM_TRAINING = False
    BATCH_NORM_DECAY = 0.95

    REGULARIZER = None

    PADDING = 'SAME'

    IS_TRAINING = False
    METRICS = False
    VERBOSE = 0
    LOGGER = DummyLogger()

    def __init__(self):
        self.input_op = None
        self.output_op = None
        self.name = 'Layer'

        self.info = LayerInfo()
        self.summaries = LayerSummaries()

    def finalize(self, output, activation=0, batch_norm=None,
                 is_training=False, batch_norm_training=False):
        if batch_norm is None:
            batch_norm = Layer.BATCH_NORM
        if activation == 0:
            activation = Layer.ACTIVATION

        if batch_norm:
            batch_norm_layer = BatchNormalization(
                output, is_training=is_training,
                batch_norm_training=batch_norm_training, name='Batch_Norm')
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('PostBatchNorm', output))
                self.summaries.scalars += batch_norm_layer.summaries.scalars
                self.summaries.histograms += batch_norm_layer.summaries.histograms
                self.summaries.images += batch_norm_layer.summaries.images
            output = batch_norm_layer.output_op
        if activation:
            output = activation(output)
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Activation', output))
        self.output_op = output


class BatchNormalization(Layer):

    def __init__(self, input_op, is_training=None, batch_norm_training=None,
                 epsilon=0.001, decay=None, name=None):
        # Perform a batch normalization after a conv layer or a fc layer
        # gamma: a scale factor
        # beta: an offset
        # epsilon: the variance epsilon - a small float number to avoid dividing by 0
        super().__init__()
        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        if decay is None:
            decay = Layer.BATCH_NORM_DECAY

        self.input_op = input_op
        input_shape = input_op.shape.as_list()[1:]

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            scale = tf.get_variable(
                'beta', shape=input_shape[-1], initializer=tf.ones_initializer(), trainable=True)
            offset = tf.get_variable(
                'gamma', shape=input_shape[-1], initializer=tf.zeros_initializer(), trainable=True)

            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('BatchNormScale', scale))
                self.summaries.histograms.append(tf.summary.histogram('BatchNormOffset', offset))

            moving_mean = tf.get_variable(
                'moving_mean', shape=input_shape[-1], initializer=tf.zeros_initializer(), trainable=False)
            moving_variance = tf.get_variable(
                'moving_var', shape=input_shape[-1], initializer=tf.ones_initializer(), trainable=False)

            if is_training:
                # Calculate the moments based on the individual batch.
                batch_mean, batch_variance = tf.nn.moments(input_op, list(range(len(input_shape) + 1)))

                update_moving_mean = tf.assign(
                    moving_mean, (moving_mean * decay) + (batch_mean * (1 - decay)))
                update_moving_variance = tf.assign(
                    moving_variance, (moving_variance * decay) + (batch_variance * (1 - decay)))

                # Allowing moving mean/variance to be update using :
                # with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
                tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_moving_mean)
                tf.add_to_collection(tf.GraphKeys.UPDATE_OPS, update_moving_variance)

                def training():
                    return tf.nn.batch_normalization(
                        input_op, batch_mean, batch_variance, offset=offset, scale=scale,
                        variance_epsilon=epsilon, name='batch_norm')

                def non_training():
                    return tf.nn.batch_normalization(
                        input_op, moving_mean, moving_variance, offset=offset, scale=scale,
                        variance_epsilon=epsilon, name='batch_norm_no_train')

                if batch_norm_training is True:
                    output = training()
                elif batch_norm_training is False:
                    output = non_training()
                else:
                    output = tf.cond(batch_norm_training, training, non_training)
            else:
                # Just use the moving_mean and moving_variance.
                output = tf.nn.batch_normalization(
                    input_op, moving_mean, moving_variance, offset=offset, scale=scale,
                    variance_epsilon=epsilon, name='batch_norm')

            self.output_op = output


class Conv1D(Layer):

    def __init__(self, input_op, output_features, name="Conv", is_training=None,
                 kernel_size=3, stride=[1, 1, 1, 1], activation=0, padding=None,
                 regularizer=0, batch_norm=None, batch_norm_training=None):
        super().__init__()

        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        if regularizer == 0:
            regularizer = Layer.REGULARIZER
        if padding is None:
            padding = Layer.PADDING

        self.input_op = input_op
        self.name = name
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]
        self.info.memory += 4 * kernel_size * input_features * output_features  # 32bits = 4 bytes
        self.info.output = 4 * (input_shape[1] / stride) * output_features

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            if is_training:
                self.weights = tf.get_variable(
                    'Kernel', shape=[kernel_size, input_features, output_features],
                    initializer=tf.truncated_normal_initializer(stddev=0.1),
                    regularizer=regularizer)
                self.biases = tf.get_variable(
                    'Bias', shape=output_features, initializer=tf.zeros_initializer(),
                    regularizer=regularizer)
            else:
                self.weights = tf.get_variable(
                    'Kernel', shape=[kernel_size, input_features, output_features],
                    initializer=tf.zeros_initializer())
                self.biases = tf.get_variable('Bias', shape=output_features, initializer=tf.zeros_initializer())
            output = tf.nn.conv1d(input_op, self.weights, stride, padding=padding) + self.biases
            if Layer.VERBOSE:
                Layer.LOGGER.info('Conv1D {} ({}x{}) : {} => {} (memory : {:.02f}KiB, output : {:.02f}KiB)'.format(
                    self.name.split('/')[-1], kernel_size, kernel_size, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Kernel', self.weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', self.biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class DeConv1D(Layer):
    def __init__(self, input_op, output_features=None, output_shape=None, name="DeConv",
                 is_training=None, kernel_size=3, stride=1, padding=None, activation=0,
                 regularizer=0, batch_norm=None, batch_norm_training=None):
        super().__init__()

        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        if regularizer == 0:
            regularizer = Layer.REGULARIZER
        if padding is None:
            padding = Layer.PADDING

        self.input_op = input_op
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]
        self.info.memory += 4 * kernel_size * input_features * output_features  # 32bits = 4 bytes
        self.info.output = 4 * (input_shape[1] * stride) * output_features

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            if not output_shape:
                output_shape = input_op.get_shape().as_list()
                output_shape[0] = tf.shape(input_op)[0]
                output_shape[1] *= stride
                output_shape[2] = output_features
            else:
                output_shape[0] = tf.shape(input_op)[0]
                if output_features is None:
                    output_features = output_shape[3]
                else:
                    output_shape[3] = output_features
            if is_training:
                self.weights = tf.get_variable(
                    'Kernel', shape=[kernel_size, output_features, input_features],
                    initializer=tf.truncated_normal_initializer(stddev=0.1),
                    regularizer=regularizer)
                self.biases = tf.get_variable(
                    'Bias', shape=output_features, initializer=tf.zeros_initializer(),
                    regularizer=regularizer)
            else:
                self.weights = tf.get_variable(
                    'Kernel', shape=[kernel_size, input_features, output_features],
                    initializer=tf.zeros_initializer())
                self.biases = tf.get_variable('Bias', shape=output_features, initializer=tf.zeros_initializer())
            output = tf.contrib.nn.conv1d_transpose(
                input_op, self.weights, output_shape, stride, padding=padding) + self.biases
            if Layer.VERBOSE:
                Layer.LOGGER.info('DeConv1D {} ({}x{}) : {} => {} (memory : {:.02f}KiB, output : {:.02f}KiB)'.format(
                    self.name.split('/')[-1], kernel_size, kernel_size, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Weights', self.weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', self.biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class Conv2D(Layer):

    def __init__(self, input_op, output_features, name="Conv", is_training=None,
                 kernel_size=3, strides=[1, 1, 1, 1], activation=0, padding=None,
                 regularizer=0, batch_norm=None, batch_norm_training=None, reuse=None):
        super().__init__()

        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        if regularizer == 0:
            regularizer = Layer.REGULARIZER
        if padding is None:
            padding = Layer.PADDING

        kernel_shape = (kernel_size, kernel_size) if isinstance(kernel_size, int) else kernel_size

        self.input_op = input_op
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]
        self.info.memory += 4 * kernel_shape[0] * kernel_shape[1] * input_features * output_features  # 32bits = 4 bytes
        self.info.output = 4 * (input_shape[1] / strides[1]) * (input_shape[2] / strides[2]) * output_features

        with tf.variable_scope(
                name if reuse else None,
                default_name=name,
                reuse=tf.AUTO_REUSE if reuse else None) as layer_scope:
            self.name = layer_scope.name
            if is_training:
                self.weights = tf.get_variable(
                    'Kernel', shape=[*kernel_shape, input_features, output_features],
                    initializer=tf.truncated_normal_initializer(stddev=0.1),
                    regularizer=regularizer)
                self.biases = tf.get_variable(
                    'Bias', shape=output_features, initializer=tf.zeros_initializer(),
                    regularizer=regularizer)
            else:
                self.weights = tf.get_variable(
                    'Kernel', shape=[*kernel_shape, input_features, output_features],
                    initializer=tf.zeros_initializer())
                self.biases = tf.get_variable('Bias', shape=output_features, initializer=tf.zeros_initializer())
            output = tf.nn.conv2d(input_op, self.weights, strides=strides, padding=padding) + self.biases
            if Layer.VERBOSE:
                Layer.LOGGER.info('{} (Conv2D {}x{}) : {} => {} (memory : {:.02f}KiB, output : {:.02f}KiB)'.format(
                    self.name.split('/')[-1], *kernel_shape, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Kernel', self.weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', self.biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class DeConv2D(Layer):
    def __init__(self, input_op, output_features=None, output_shape=None, name="DeConv",
                 is_training=None, kernel_size=3, strides=[1, 1, 1, 1], padding=None, activation=0,
                 regularizer=0, batch_norm=None, batch_norm_training=None):
        super().__init__()

        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        if regularizer == 0:
            regularizer = Layer.REGULARIZER
        if padding is None:
            padding = Layer.PADDING

        self.input_op = input_op
        input_shape = input_op.get_shape().as_list()
        input_features = input_shape[-1]

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            if not output_shape:
                output_shape = input_op.get_shape().as_list()  # [?, 32, 32, output_features]
                output_shape[0] = tf.shape(input_op)[0]
                output_shape[1] *= strides[1]
                output_shape[2] *= strides[2]
                output_shape[3] = output_features
            else:
                output_shape[0] = tf.shape(input_op)[0]
                if output_features is None:
                    output_features = output_shape[3]
                else:
                    output_shape[3] = output_features
            self.info.memory += 4 * kernel_size * kernel_size * input_features * output_features  # 32bits = 4 bytes
            self.info.output = 4 * output_shape[1] * output_shape[2] * output_features
            if is_training:
                self.weights = tf.get_variable(
                    'Kernel', shape=[kernel_size, kernel_size, output_features, input_features],
                    initializer=tf.truncated_normal_initializer(stddev=0.1),
                    regularizer=regularizer)
                self.biases = tf.get_variable(
                    'Bias', shape=output_features, initializer=tf.zeros_initializer(),
                    regularizer=regularizer)
            else:
                self.weights = tf.get_variable(
                    'Kernel', shape=[kernel_size, kernel_size, output_features, input_features],
                    initializer=tf.zeros_initializer())
                self.biases = tf.get_variable('Bias', shape=output_features, initializer=tf.zeros_initializer())
            output = tf.nn.conv2d_transpose(
                input_op, self.weights, output_shape, strides, padding=padding) + self.biases
            if Layer.VERBOSE:
                Layer.LOGGER.info('{} (DeConv2D {}x{}) : {} => {} (memory : {:.02f}KiB, output : {:.02f}KiB)'.format(
                    self.name.split('/')[-1], kernel_size, kernel_size, str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Kernel', self.weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', self.biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class FullyConnected(Layer):

    def __init__(self, input_op, output_dimmension, name='FC', is_training=None,
                 activation=0, regularizer=0, batch_norm=None, batch_norm_training=None):
        super().__init__()

        if is_training is None:
            is_training = Layer.IS_TRAINING
        if batch_norm_training is None:
            batch_norm_training = Layer.BATCH_NORM_TRAINING
        if regularizer == 0:
            regularizer = Layer.REGULARIZER

        self.input_op = input_op
        input_dimmension = input_op.get_shape().as_list()[-1]
        self.info.memory = 4 * input_dimmension * output_dimmension  # 32bits = 4 bytes
        self.info.output = 4 * output_dimmension  # 32bits = 4 bytes
        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            if is_training:
                self.weights = tf.get_variable(
                    'Kernel', shape=[input_dimmension, output_dimmension],
                    initializer=tf.truncated_normal_initializer(stddev=0.1),
                    regularizer=regularizer)
                self.biases = tf.get_variable(
                    'Bias', shape=output_dimmension, initializer=tf.zeros_initializer(),
                    regularizer=regularizer)
            else:
                self.weights = tf.get_variable(
                    'Kernel', shape=[input_dimmension, output_dimmension],
                    initializer=tf.zeros_initializer())
                self.biases = tf.get_variable('Bias', shape=output_dimmension, initializer=tf.zeros_initializer())
            output = tf.matmul(input_op, self.weights) + self.biases
            if Layer.VERBOSE:
                Layer.LOGGER.info('{} (FC) : {} => {} (memory : {:.02f}KiB, output : {:.02f}KiB)'.format(
                    self.name.split('/')[-1], str(input_op.shape), str(output.shape),
                    self.info.memory / 1024, self.info.output / 1024))
            if Layer.METRICS:
                self.summaries.histograms.append(tf.summary.histogram('Weights', self.weights))
                self.summaries.histograms.append(tf.summary.histogram('Biases', self.biases))
                self.summaries.histograms.append(tf.summary.histogram('Output', output))

            self.finalize(output, activation=activation, batch_norm=batch_norm,
                          is_training=is_training, batch_norm_training=batch_norm_training)


class LSTM(Layer):

    def __init__(self, output_dimmension, name="LSTM", activation=0):
        super().__init__()

        # input_dimmension = input_op.shape[-1]
        # self.info.memory = 4 * input_dimmension * ouput_dimmension  # 32bits = 4 bytes

        with tf.variable_scope(None, default_name=name) as layer_scope:
            self.name = layer_scope.name
            self.output_op = tf.contrib.rnn.BasicLSTMCell(output_dimmension, activation=activation)
