from typing import List

# import tensorflow as tf
import src.tf1_utils.utils.import_tf1 as tf

from . import layers as layers


class Network():

    def __init__(self, input_op: tf.Tensor):
        self.input_op = input_op
        self.layers: List[layers.Layer] = []
        self.output_op = None

    def conv1D(self, input_op, output_features, **kwargs):
        convolution = layers.Conv1D(input_op, output_features, **kwargs)
        self.layers.append(convolution)
        return convolution.output_op

    def deconv1D(self, input_op, **kwargs):
        deconvolution = layers.DeConv1D(input_op, **kwargs)
        self.layers.append(deconvolution)
        return deconvolution.output_op

    def conv2D(self, input_op, output_features, **kwargs):
        convolution = layers.Conv2D(input_op, output_features, **kwargs)
        self.layers.append(convolution)
        return convolution.output_op

    def deconv2D(self, input_op, **kwargs):
        deconvolution = layers.DeConv2D(input_op, **kwargs)
        self.layers.append(deconvolution)
        return deconvolution.output_op

    def skip_connection(self, input_op, skip_op, output_features=None, stop_grad=False, name="SkipCon", **kwargs):
        output_features = output_features if output_features is not None else skip_op.shape.as_list()[-1]
        with tf.compat.v1.variable_scope(None, default_name=name):
            if stop_grad:
                skip_op = tf.stop_gradient(skip_op)
            output_op = tf.concat([input_op, skip_op], axis=-1)
            convolution = layers.Conv2D(output_op, output_features, name="1x1", kernel_size=1, **kwargs)
            self.layers.append(convolution)
        return convolution.output_op

    def fully_connected(self, input_op, output_dimmension, **kwargs):
        fc = layers.FullyConnected(input_op, output_dimmension, **kwargs)
        self.layers.append(fc)
        return fc.output_op
